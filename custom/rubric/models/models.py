# -*- coding: utf-8 -*-

from odoo import models, fields, api


class rubric(models.Model):
    _name = 'rubric.rubric'
    _description = 'rubric.rubric'

    title = fields.Text(required = True)
    excellent = fields.Text(required = True)
    good = fields.Text(required = True)
    needs_improvement = fields.Text(required = True)
    poor = fields.Text(required = True)
