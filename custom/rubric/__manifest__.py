# -*- coding: utf-8 -*-
{
    'name': "rubric",

    'summary': """A demo showing Rubrics App""",

    'description': """
        Already Described In Summary
    """,

    'author': "Code Asterisk Private Limited",
    'website': "https://www.codeasterisk.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Custom',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable' : True,
    'application': True,
    'auto_install': False,
}
