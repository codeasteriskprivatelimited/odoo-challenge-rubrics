# -*- coding: utf-8 -*-
from odoo import http


class Rubric(http.Controller):
    @http.route('/rubrics', auth='public',website=True)
    def list(self, **kw):
        rubrics = http.request.env['rubric.rubric'].search([])
        return http.request.render('rubric.listing', {
            # 'root': '/rubric/rubric',
            'objects': rubrics,
        })
